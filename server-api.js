const jsonServer = require('json-server');
const server = jsonServer.create();
const router = jsonServer.router('db.json');
const middlewares = jsonServer.defaults();
const bodyParser = require('body-parser');
const cors = require('cors'); // Import CORS middleware

server.use(middlewares);
server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());
server.use(cors()); // Use CORS middleware to enable CORS

// Custom login endpoint
server.post('/login', (req, res) => {
  const { email, password } = req.body;
  const user = router.db.get('users').find({ email, password }).value();

  if (user) {
    res.json({ token: user.token });
  } else {
    res.status(401).json({ error: 'Invalid credentials' });
  }
});

server.use(router);

const PORT = 3002; // Adjust the port if needed
server.listen(PORT, () => {
  console.log(`JSON Server is running on port ${PORT}`);
});
