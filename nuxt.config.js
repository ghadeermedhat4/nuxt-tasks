export default {
  // Other configurations...

  // Enable Vuex Store
  store: true,

  // Global page headers
  head: {
    title: 'My Nuxt App',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS
  css: [],

  // Plugins to run before rendering page
  plugins: [
    { src: '~/plugins/hello.js', mode: 'client' },
    '~/plugins/vuelidate.js',
  ],

  // Auto import components
  components: true,

  // Modules for dev and build (recommended)
  buildModules: [],

  // Modules
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/i18n',
    '@nuxtjs/vuetify',
    '@nuxtjs/auth-next',
  ],

  i18n: {
    locales: [
      { code: 'en', iso: 'en-US', name: 'English', file: 'en.json' },
      { code: 'ar', iso: 'ar-SA', name: 'العربية', file: 'ar.json' }
    ],
    langDir: 'locales/',
  },

  // Axios module configuration
  axios: {
    baseURL: 'https://jsonplaceholder.typicode.com',
  },

  vuetify: {
    customVariables: ['~/assets/variables.scss'],
  },

  // Auth module configuration
  auth: {
    strategies: {
      local: {
        token: {
          property: 'token',
          global: true,
          type: 'Bearer'
        },
        user: {
          property: false,
        },
        endpoints: {
          login: { url: '/users', method: 'get' }, // Use GET for testing
          logout: false,
          user: { url: '/users/1', method: 'get' } // Fetch user with id 1
        },
        tokenType: '',
        redirect: {
          login: '/login',
          logout: '/',
          home: '/fetchdata'
        }
      }
    }
  },

  // Build Configuration
  build: {}
}
